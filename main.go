package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"path"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

const (
	droneDir = "/var/lib/drone"
	dbFile   = "database.sqlite"
	logDir   = "dronelogs/logs"
)

func main() {
	log.Println("starting...")

	if err := os.MkdirAll(logDir, os.ModePerm); err != nil {
		log.Fatal(err)
	}

	filename := path.Join(droneDir, dbFile)

	start := time.Now()

	log.Printf("logs dir: %s\n", logDir)
	log.Printf("opening db file: %s\n", filename)

	db, err := sql.Open("sqlite3", "file:"+filename+"?mode:ro")
	if err != nil {
		log.Fatal(err)
	}

	defer db.Close()

	rows, err := db.Query("SELECT * FROM logs")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var id int
	var row string

	for rows.Next() {

		err = rows.Scan(&id, &row)

		if err != nil {
			log.Fatal(err)
		}

		log.Printf("handling row id: %d\n", id)

		f, err := os.Create(path.Join(logDir, fmt.Sprint(id)))
		if err != nil {
			log.Fatal(err)
		}
		defer f.Close()

		log.Println("saving row to a file")

		_, err = f.Write([]byte(row))
	}

	elapsed := time.Since(start)

	log.Printf("took: %s", elapsed)
}
